/**
 * ATM Interface defines contract for banking operations. According to the spec, floats must be used.
 * 
 * @author Bret Fontecchio
 * @since November 14, 2013
 */
 
package cscie55.hw4;

public interface ATM {
	public void deposit(float amount) throws ATMException;
	public void withdraw(float amount) throws ATMException;
	public Float getBalance() throws ATMException;
}