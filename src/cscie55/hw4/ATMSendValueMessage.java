/**
 * ATMSendValueMessage stores a BigDecimal amount, and can easily convert between floats,
 * Strings, and BigDecimal amounts by wrapping BigDecimal methods. To implement rounding 
 * and rescaling, modify getValue(), which is chained to converters such as getFloatValue().
 * 
 * This will be extended to include account details in version 2.0. Authentication is the next
 * logical extension.
 * 
 * @see ATMProtocol
 */

package cscie55.hw4;

import java.math.BigDecimal;

import cscie55.hw4.ATMProtocol.ATMLexicon;

	abstract class ATMSendValueMessage extends ATMMessage {
		protected BigDecimal value;
		/**
		 * Returns <code>value</code>.
		 * If you want to round something that came from the network, do it here.
		 * @return value	The value stored in this ATMSendValueMessage
		 */
		public BigDecimal getValue() {
			return value;
		}
		/**
		 * If you want to round before sending across socket, do it here. Rounding here would
		 * lower network traffic (somewhat).
		 * 
		 * @param 	f	The float to convert
		 * @return	BigDecimal conversion of f
		 */
		public BigDecimal floatToBigDecimal(float f) {
			return new BigDecimal(Float.toString(f));
		}
		/**
		 * @return <code>value</code> as a float
		 */
		public float getFloatValue() {
			return value.floatValue();
		}
		/**
		 * @return <code>value</code> as a scaled String
		 */
		public String getStringValue() {
			return value.setScale(2).toPlainString();
		}
	}
	
	/**
	 * A deposit message, carrying the BigDecimal amount to be deposited.
	 *
	 */
	class DepMessage extends ATMSendValueMessage{
		public DepMessage(float floatValue) {
			this.value = floatToBigDecimal(floatValue);
			this.identifier = ATMLexicon.DEP;
		}
		public DepMessage(BigDecimal value) {
			this.value = value;
			this.identifier = ATMLexicon.DEP;
		}
		String serialize() {
			String msg = String.format("DEP:%s:", getStringValue());
			return msg;
		}
		ATMMessage execute(ATM atm) throws ATMException {
			try {
				atm.deposit(this.getFloatValue());
			} catch (ATMException e) {
				return new NakMessage();
			}
			return new AckMessage();
		}
	}
	
	/**
	 * A withdraw message, carrying the BigDecimal amount to be withdrawn.
	 *
	 */
	class WthMessage extends ATMSendValueMessage {
		public WthMessage(float floatValue) {
			this.value = floatToBigDecimal(floatValue);
			identifier = ATMLexicon.WTH;
		}
		public WthMessage(BigDecimal value) {
			this.value = value;
			identifier = ATMLexicon.WTH;
		}
		String serialize() {
			String msg = String.format("WTH:%s:", getStringValue());
			return msg;
		}
		ATMMessage execute(ATM atm) throws ATMException {
			try {
				atm.withdraw(this.getFloatValue());
			} catch (ATMException e) {
				return new NakMessage();
			}
			return new AckMessage();
		}
	}
	
	/**
	 * A balance response message, carrying the BigDecimal amount in the account.
	 *
	 */
	class BrpMessage extends ATMSendValueMessage {
		public BrpMessage(float floatValue) {
			this.value = floatToBigDecimal(floatValue);
			this.identifier = ATMLexicon.BRP;
		}
		public BrpMessage(BigDecimal amount) {
			this.value = amount;
			this.identifier = ATMLexicon.BRP;
		}
		String serialize() {
			String msg = String.format("BRP:%s:", getStringValue());
			return msg;
		}
		ATMMessage execute(ATM atm) throws ATMException {
			throw new ATMException("Message Incongruity: Server was given"
					+ " a balance response from client.");
		}
	}