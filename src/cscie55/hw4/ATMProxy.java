/**
 * ATMProxy acts as proxy between an ATM client and an ATM server. ATMProxy connects
 * to the ATM server, passing it Strings representing ATMMessage objects serialized
 * in accordance with ATMProtocol. Connections are done over sockets.
 * Version 2.0 vill be multi-threaded.
 * 
 * This version returns a float, in accordance with the spec, but a scaled BigDecimal
 * value could be easily overloaded.
 * 
 * @version 1.0
 * @see cscie55.hw4.ATMProtocol.java
 */ 

package cscie55.hw4;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

import cscie55.hw4.ATMProtocol;

public class ATMProxy implements ATM {
	
	/* Constants */
	static private final int DEFAULT_PORT = 8889;
	static private final String DEFAULT_SERVER = "localhost";
	static private final int DEFAULT_TIMEOUT_MILLIS = 5000;
	static private final int NUM_CONNECT_RETRIES = 10;
	static private final boolean VERBOSE = false;
	
	/* Top Level Members */
	/** 
	 * remote (Server) hostname
	 * @see <code>Server</code> */
	private String host;
	/** 
	 * remote (Server) port
	 * @see <code>Server</code> */
	private int port;
	/** 
	 * this gets passed to <code>Socket.setSoTimeout</code> in <code>connect()</code> */
	private int timeout;
	/** 
	 * We only have one socket because ATMProxy will be instantiated for each Client 
	 */
	private Socket socket = null;
	/** 
	 * Output. See Implementation notes on PRINTWRITER. */
	private PrintWriter socketWriter = null;
	/** 
	 * Line-by-line Input. */
	private Scanner socketReader = null;
	
	/**
	 * Default ATMProxy Constructor
	 * 
	 * @throws ATMException
	 */
	public ATMProxy() throws ATMException {
		this.host = DEFAULT_SERVER;
		this.port = DEFAULT_PORT;
		this.timeout = DEFAULT_TIMEOUT_MILLIS;
		connect();
	}
	
	public ATMProxy(String host) throws ATMException{
		this.host = host;
		this.port = DEFAULT_PORT;
		this.timeout = DEFAULT_TIMEOUT_MILLIS;
		connect();
	}
	
	/**
	 * Preferred ATMProxy constructor.
	 * 
	 * @param host	remote (Server) hostname
	 * @param port	remote (Server) port
	 * @throws ATMException
	 */
	public ATMProxy(String host, int port) throws ATMException {
		this.host = host;
		this.port = port;
		connect();
	}
	
	/**
	 * ATMProxy constructor.
	 * 
	 * @param host		remote (Server) hostname
	 * @param port		remote (Server) port
	 * @param timeout	socket timeout in milliseconds
	 * @throws ATMException
	 */
	public ATMProxy(String host, int port, int timeout) throws ATMException {
		this.host = host;
		this.port = port;
		this.timeout = timeout;
		connect();
	}

	/**
	 * Issues <code>DepMessage</code> over the network, waits for <code>AckMessage</code>. 
	 * IO is Blocking.
	 * 
	 * Note: this will not timeout in <code>timeout</code> millis. That might be a good feature
	 * though.
	 */
	@Override
	public void deposit(float amount) throws ATMException {
		
		ATMMessage outMsg = new DepMessage(amount);
		ATMMessage inMsg = talkToServer(outMsg);
		switch(inMsg.getIdentifier()) {
		case ACK: return;
		case NAK: throw new ATMException("Server declined deposit");
		case BYE: {
			try {
				hangup(true);
			} catch (ATMException e) {
				throw new ATMException("Exception hanging up from BYE response in Responder");
			}
		}
		default: throw new ATMException("Server response incongruent with request");
		}
	}
	
	/**
	 * Issues <code>WthMessage</code> over the network, waits for <code>AckMessage</code>. 
	 * @see ATMSendValueMessage
	 * IO is Blocking.
	 * 
	 * Note: this will not timeout in <code>timeout</code> millis. That might be a good feature
	 * though.
	 */
	@Override
	public void withdraw(float amount) throws ATMException {
		ATMMessage outMsg = new WthMessage(amount);
		ATMMessage inMsg = talkToServer(outMsg);
		switch(inMsg.getIdentifier()) {
		case ACK: return;
		case NAK: throw new ATMException("Server declined withdrawal");
		case BYE: {
			try {
				hangup(true);
			} catch (ATMException e) {
				throw new ATMException("Exception hanging up from BYE response in Responder");
			}
		}
		default: throw new ATMException("Server response incongruent with request");
		}
	}
	
	
	/**
	 * Issues  <code>DepMessage</code> over the network, waits for <code>BrpMessage</code>.
	 * @see ATMMessage
	 * IO is Blocking.
	 * 
	 * I would have done BigDecimal getBalance(), using a round then rescale operation here.
	 * See implementation notes.
	 * 
	 * Note: this will not timeout in <code>timeout</code> millis. That might be a good feature
	 * though.
	 */
	@Override
	public Float getBalance() throws ATMException {
		log("Getting balance.");
		ATMMessage outMsg = new BchMessage();
		ATMMessage inMsg = talkToServer(outMsg);
		log("Server responded");
		switch(inMsg.getIdentifier()) {
		case BRP: {
			if (inMsg instanceof ATMSendValueMessage) {
				return ((ATMSendValueMessage) inMsg).getFloatValue();
			}
		}
		case NAK: throw new ATMException("Server declined balance update", true);
		case BYE: {
			try {
				hangup(true);
			} catch (ATMException e) {
				throw new ATMException("Exception hanging up from BYE"
						+ " response in Responder", true);
			}
		}
		default: throw new ATMException("Server response incongruent with request", true);
		}
	}
	
	/**
	 * Breaks the loop after one line read and responded to. Blocks in the meantime
	 * 
	 * @param outMsg
	 */
	private ATMMessage talkToServer(ATMMessage outMsg) throws ATMException{
		socketWriter.println(outMsg.serialize());
		log("Sending: " + outMsg.serialize());
		ATMMessage incomingMessage = null;
		while((socketReader.hasNext())) {
			incomingMessage = ATMProtocol.getATMMessage(socketReader.nextLine());
			break;
		}
		return incomingMessage;
	}
	
	/**
	 * Tries (up to several times) to create a socket connection to the <code>Server</code>. 
	 * @throws ATMException
	 */
	private void connect() throws ATMException {
		int failures = 0;
		while (failures < NUM_CONNECT_RETRIES && socket == null) {
			try {
				socket = new Socket(host, port);
				socket.setSoTimeout(timeout);
				socketWriter = new PrintWriter(socket.getOutputStream(), true);
				socketReader = new Scanner(socket.getInputStream());
			} catch (UnknownHostException e) {
				hangup(false);
				throw new ATMException("Unknown Host.", true);
			} catch (SocketException e) {
				hangup(false);
				throw new ATMException("Socket problem. "
						+ "Server might not be running.", true);
			} catch (IOException e) {
				hangup(false);
				throw new ATMException("IOException.", true);
			} 
		}
	}
	
	/** Exposed convenience wrapper for hangup(boolean)
	 * Close socket connection. Says "BYE" if possible. 
	 * @throws ATMException
	 */
	public void hangup() throws ATMException {
		hangup(false);
	}
	
	/**
	 * Close socket connection. Says "BYE" if possible. 
	 * @param remoteHungUp
	 * @throws ATMException
	 */
	private void hangup(boolean remoteHungUp) throws ATMException {
		if (socket != null) {
			// say bye
			socketWriter.print(new ByeMessage().serialize());
			if (socket.isConnected() && !(socket.isClosed())) {
				talkToServer(new ByeMessage());
				// close socket
				try {
					socket.close();
				} catch (IOException e) {
					throw new ATMException("IOException during hangup.", true);
				}
			}
		}
		// close resources
		if (socketWriter != null)
			socketWriter.close();
		if (socketReader != null)
			socketReader.close();
		if (remoteHungUp) {
			log("Server said BYE. Connection closed.");
		}
	}
	
	/**
	 * log a message to System.out
	 * This will come up on the console for whoever ran <code>Client</code>.
	 * @param s
	 */
	protected static void log(String s) {
		if (VERBOSE) {
			// unlike Server, client has no timestamp
			System.out.println(s);
		}
	}
	
	/**
	 * Override Object's finalize method to safeguard against resource leaks when ATMProxy goes
	 * to the Garbage Collector. 
	 *
	protected void finalize() throws ATMException {
		try {
			hangup(false);
		} catch (Throwable t) {
			throw new ATMException("Couldn't close resources in ATMProxy finalize", true);
		} finally {
			try {
				super.finalize();
			} catch (Throwable e) {
				e.printStackTrace();
				throw new ATMException("Couldn't finalize ATMProxy.super", true);
			}
		}
	}
	* This might have to be called manually, not by the garbage collector
	* The try-catches don't need to be copied over to hangup if we're using hangup for finalization
	* because those are only there due to these methods re-throwing what they catch.
	* 
	* see: http://stackoverflow.com/questions/5635749/what-is-the-use-of-overriding-the-finalize-method-of-object-class
	*
	*/
}