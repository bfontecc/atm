package cscie55.hw4;


import cscie55.hw4.ATMProtocol.ATMLexicon;

/**
 * ATMMessage is a simple class implementing ATMProtocol specifications.
 *
 * @see ATMProtocol
 */

	abstract class ATMMessage {
		protected ATMLexicon identifier = null;
		public ATMLexicon getIdentifier() {
			return identifier;
		}
		/** return a String formatted according to ATMProtocol */
		abstract String serialize();
		abstract ATMMessage execute(ATM atm) throws ATMException;
	}
	
	/** Acknowledge */
	class AckMessage extends ATMMessage {
		public AckMessage() {
			this.identifier = ATMLexicon.ACK;
		}
		String serialize() {
			return "ACK:";
		}
		ATMMessage execute(ATM atm) { return null; }
	}
	
	/** Negative Acknowledge */
	class NakMessage extends ATMMessage {
		public NakMessage() {
			this.identifier = ATMLexicon.NAK;
		}
		String serialize() {
			return "NAK:";
		}
		ATMMessage execute(ATM atm) { return null; }
	}
	
	/** Bye */
	class ByeMessage extends ATMMessage {
		public ByeMessage() {
			this.identifier = ATMLexicon.BYE;
		}
		String serialize() {
			String msg = String.format("BYE:");
			return msg;
		}
		ATMMessage execute(ATM atm) { return null; }
	}
	
	/** Balance Check */
	class BchMessage extends ATMMessage {
		public BchMessage() {
			this.identifier = ATMLexicon.BCH;
		}
		String serialize() {
			String msg = String.format("BCH:");
			return msg;
		}
		ATMMessage execute(ATM atm) throws ATMException {
			try {
				return new BrpMessage(atm.getBalance());
			} catch (ATMException e) {
				throw new ATMException("Couldn't get balance response message");
			}
		}
	}
	
	