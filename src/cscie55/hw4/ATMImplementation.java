package cscie55.hw4;

public class ATMImplementation implements ATM {
	
	/**
	 * For now we're just using one account
	 */
	Account account;
	
	public ATMImplementation() throws ATMException {
		account = new Account(0.0f);
	}

	/**
	 * Deposit non-negative floating point number into account.
	 * 
	 * Throw ATMException on NaN or +/- Infinity. Watch for overflow. Disallow any negative number. 
	 * 
	 * @param amount	non-negative floating point number
	 */
	@Override
	public void deposit(float amount) throws ATMException {
		float bal = account.getBalance();
		digestPoison(amount);
		if (amount < 0) {
			throw new ATMException("Cannot deposit negative value");
		}
		if (bal < 0) {
			// safe from overflow, and underflow would involve subtraction
			account.setBalance(bal + amount);
		} else {
			if ((Float.MAX_VALUE - amount) > bal ) {
				account.setBalance(bal + amount);
			}
		}
	}

	/**
	 * Withdraw non-negative floating point number from account.
	 * 
	 * Throw ATMException on NaN or +/- Infinity. Watch for underflow. Disallow any negative number. 
	 * 
	 * @param amount	non-negative floating point number
	 */
	@Override
	public void withdraw(float amount) throws ATMException {
		float bal = account.getBalance();
		digestPoison(amount);
		if (amount < 0) {
			throw new ATMException("Cannot withdraw negative amount.");
		}
		if (bal > 0) {
			// safe from underflow
			account.setBalance(bal - amount);
		} else {
			if ((-1 * Float.MAX_VALUE + amount) < bal) {
				account.setBalance(bal - amount);
			}
		}
	}
	
	/**
	 * @return balance
	 */
	@Override
	public Float getBalance() throws ATMException {
		return account.getBalance();
		/** Rounding here would effect the BigDecimal value send along the network,
		 * as well as the presented value the client sees. */
	}
	
	/**
	 * Check floating point number for poison values NaN (Not a Number), and Infinity (positive or negative)
	 */
	private void digestPoison(float amount) throws ATMException {
		if (amount == Float.NaN || amount == Float.NEGATIVE_INFINITY || amount == Float.POSITIVE_INFINITY) {
			throw new ATMException("Amount is poison value " + Float.toString(amount));
		}
		if ((amount + Math.ulp(amount)) > Float.MAX_VALUE || (amount - Math.ulp(amount)) < Float.MIN_VALUE) {
			throw new ATMException("Float is invalid amount.");
		}
	}

}
