/**
 * Account stores information relating to a particular user's account. It could be extended to include transaction information, but for
 * now, it just wraps a float. Floats were specified in the spec, otherwise BigDecimal would have been good here. As of version 2.0, there
 * is an accountNumber field.
 */

package cscie55.hw4;

public class Account {
	private float balance;
	
	public Account(float balance) throws ATMException {
		if (Float.isInfinite(balance) || Float.isNaN(balance)) {
			throw new ATMException("Impossible account balance.");
		}
		this.balance = balance;
	}
	
	protected void setBalance(float updated) throws ATMException {
		if (Float.isInfinite(updated) || Float.isNaN(updated)) {
			throw new ATMException("Impossible account balance.");
		}
		balance = updated;
	}
	
	public float getBalance() {
		return balance;
	}
}