/**
 * A message to or from an ATMProtocol should:
 * 
 * (1) be a String ending with a newline character
 * 	a.) newline character is given by serialize methods in ATMMessage
 * 		and should not be added explicitly. Doing so could yield empty message.
 * (2) begin with a valid ATMLexicon lexeme e.g. "ACK" or "DEP"
 * (3) have no newline characters in the message content
 * (4) use ATMProtocol.delimiter between token and argument
 * (5) use ATMProtocol.delimiter between arg n-1 and arg n for all n args
 * (6) use delimiter between arg n and newline
 * (7) make sure arguments match constructor for that ATMMessage
 * (8) use BigDecimal Strings as the numerical quantities (no quotes on strings)
 * (9) end in a newline... but beware PrintWriter.println does this for you, and 
 * 		two newlines will lead to a empty message being picked up
 * 
 * example strings:
 * 		WTH:100.00:\n
 * 		ACK:\n
 *  
 * @author Bret Fontecchio
 *
 */

package cscie55.hw4;

import java.math.BigDecimal;
import java.util.*;

import cscie55.hw4.ATMException;

public class ATMProtocol {
	
	/**
	 * An enumeration of Factory objects for building ATMMessage objects using their (somewhat varied)
	 * constructors. Enforces a finite number of instances, essentially a singleton for each identifier.
	 */
	public static enum ATMLexicon {
		/** Acknowledge */
		ACK {
			public ATMMessage buildMessage(String... fields) {
				// field is probably '\n', could be null. It doesn't matter, we don't need it.
				return new AckMessage();
			}
		},
		/** Negative Acknowledge */
		NAK {
			public ATMMessage buildMessage(String... fields) {
				return new NakMessage();
			}
		},
		/** Deposit (with value) */
		DEP {
			public ATMMessage buildMessage(String... fields) throws ATMException {
				return new DepMessage(getValueBD(fields[0]));
			}
		},
		/** Withdraw (with value) */
		WTH {
			public ATMMessage buildMessage(String... fields) throws ATMException {
				return new WthMessage(getValueBD(fields[0]));
			}
		},
		/** Check Balance */
		BCH {
			public ATMMessage buildMessage(String... fields) {
				return new BchMessage();
			}
		},
		/** Balance Response (with value) */
		BRP {
			public ATMMessage buildMessage(String... fields) throws ATMException {
				return new BrpMessage(getValueBD(fields[0]));
			}
		},
		/** Bye Hanging Up */
		BYE {
			public ATMMessage buildMessage(String... fields ) {
				return new ByeMessage();
			}
		};
		/**
		 * 
		 * @param field			Field containing value data in the message. May be null or '\n'
		 * @return message		<? extends ATMMessage> object
		 */
		abstract ATMMessage buildMessage(String... fields) throws ATMException;
	}
	
	public static final String delimiter = ":";
	
	/**
	 * Bridge between String and <code>buildMessage</code>. Extracts identifier and value
	 * from String and attempts to create ATMMessage using <code>ATMLexicon</code>
	 */
	public static ATMMessage getATMMessage(String msg) throws ATMException {
		ATMMessage atmMessage;
		ATMLexicon messageStrategy;
		/** Ack is an example of an identifier */
		String identifier;
		/** 10.00 is an example of a field */
		String field;
		StringTokenizer tokenizer = new StringTokenizer(msg, delimiter);
		if (!tokenizer.hasMoreTokens()) {
			throw new ATMException("Received Empty String.");
		}
		identifier = tokenizer.nextToken();
		messageStrategy = Enum.valueOf(ATMLexicon.class, identifier);
		if (!tokenizer.hasMoreTokens()) {
			atmMessage = messageStrategy.buildMessage();
		} else {
			field = tokenizer.nextToken();
			atmMessage = messageStrategy.buildMessage(field);
		}
		messageStrategy = Enum.valueOf(ATMLexicon.class, identifier);
		return atmMessage;
	}
	
	/**
	 * Extracts the value from a String value field from a (well-formed and serialized) 
	 * ATMSendValueMessage. <code>field</code> Should be a BigDecimal String.
	 * 
	 * @see getATMMessage
	 * 
	 * @param field		the String we believe has a BigDecimal value
	 * @return value	the BigDecimal object
	 * @throws ATMException
	 */
	private static BigDecimal getValueBD(String field) throws ATMException {
		BigDecimal bd = null;
		try {
			bd = new BigDecimal(field);
		} catch (NumberFormatException ex) {
			throw new ATMException("Couldn't create BigDecimal from " + "\"" + field +"\"");
		} 
		return bd;
	}
}
