package cscie55.hw4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ATMImplementationTest {
	
	/** test getBalance 
	 * @throws ATMException */
	@Test
	public void getBalanceTest() throws ATMException {
		ATMImplementation atm = new ATMImplementation();
		assertEquals(0.0f,atm.getBalance(), 0.001f);
	}
	
	/** test deposit
	 * @throws ATMException */
	 @Test
	 public void depositTest() throws ATMException {
		 ATMImplementation atm = new ATMImplementation();
		 float origBalance = atm.getBalance();
		 atm.deposit(1000.0f);
		 float newBalance = atm.getBalance();
		 assertEquals(1000.0f, newBalance - origBalance, .000001);
	 }
	 
	 /** test withdraw
	  * @throws ATMException */
	 @Test
	 public void withdrawTest() throws ATMException {
		 ATMImplementation atm = new ATMImplementation();
		 float origBalance = atm.getBalance();
		 atm.withdraw(1000.0f);
		 float newBalance = atm.getBalance();
		 assertEquals(1000.0f, origBalance - newBalance, .000001);
	 }
}
