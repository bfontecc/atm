package cscie55.hw4;

import org.junit.Test;

public class AccountTest {
	
	/**
	 * Make sure invalid float values can't be passed to Account constructor
	 */
	
	@Test(expected=ATMException.class)
	public void testNaNConstruction() throws ATMException{
		@SuppressWarnings("unused")
		Account A = new Account(Float.NaN);
	}
	
	@Test(expected=ATMException.class)
	public void testInfinityBalance() throws ATMException{
		@SuppressWarnings("unused")
		Account A = new Account(Float.POSITIVE_INFINITY);
	}
	
	@Test(expected=ATMException.class)
	public void testNegInfinityBalance() throws ATMException{
		@SuppressWarnings("unused")
		Account A = new Account(Float.NEGATIVE_INFINITY);
	}
	
	/**
	 * Make sure invalid floats can't be sent to <code>setBalance</code>
	 */
	
	@Test(expected=ATMException.class)
	public void testNaNSet() throws ATMException{
		Account A = new Account(0.0f);
		A.setBalance(Float.NaN);
	}
	
	@Test(expected=ATMException.class)
	public void testInfinitySet() throws ATMException{
		Account A = new Account(0.0f);
		A.setBalance(Float.POSITIVE_INFINITY);
	}
	@Test(expected=ATMException.class)
	public void testNegInfinitySet() throws ATMException{
		Account A = new Account(0.0f);
		A.setBalance(Float.NEGATIVE_INFINITY);
	}
	
}
